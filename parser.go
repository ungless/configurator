package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// OpenMasterFile() returns an os.File of the master file
func OpenMasterFile() *os.File {
	openedFile, err := os.Open(masterFile)
	if err != nil {
		log.Fatal(err)
	}
	return openedFile
}

// GetMasterFileContents() returns the entire contents of the master file as a string
func GetMasterFileContents() string {
	masterFileContents, err := ioutil.ReadFile(masterFile)
	if err != nil {
		log.Fatal(err)
	}
	return string(masterFileContents)
}

// GetLines() splits a given string (a file) by newlines
func GetLines(fileContent string) []string {
	lines := strings.Split(fileContent, "\n")
	return lines
}

// GetSetting() extracts the contents of a given setting
func GetSetting(settingName string) string {
	masterFileContents := GetMasterFileContents()
	lines := GetLines(masterFileContents)
	fmt.Println(lines)

	for _, setting := range lines {
		fullSetting := strings.Split(setting, ": ")
		fileSettingName := fullSetting[0]
		fileSettingContent := fullSetting[1]
		if fileSettingName == settingName {
			return fileSettingContent
		}
	}
	return "Could not find" + settingName
}

func RetrieveConfigs() {}

func RetrieveConfigSettings(configName string) {}
